class ApplicationController < ActionController::Base
  require 'net/http'
  require 'json'

  def bitcoinRPC(method, param)
    r=`bitcoin-cli #{method} #{param}`.chomp
    if r.class == String then
       return r
    else
       return JSON.parse(r)
    end
  end

  def sendfromRPC(address, amount)
    `bitcoin-cli sendtoaddress #{address} #{amount}`.chomp
  end
end
