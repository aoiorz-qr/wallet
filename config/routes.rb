Rails.application.routes.draw do
  root 'passbooks#index'
  get '/index', to: 'passbooks#index'
  get '/sendbit', to: 'passbooks#sendbit'
  get '/transfer', to: 'passbooks#transfer'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
